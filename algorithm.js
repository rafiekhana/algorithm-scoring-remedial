// -   `countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]))` => 7
// -   `countUniqueValues([1, 2, 2, 2, 2, 2, 2, 2, 4, 6]))` => 4
// -   `countUniqueValues([]))` => 0

function countUniqueValues(arr) {
  let unique = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] !== (arr[i+1]))
    console.log(arr[i]);
    unique++;
  }
}


console.log(countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]));